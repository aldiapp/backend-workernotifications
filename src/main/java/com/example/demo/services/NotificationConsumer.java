/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.services;

import com.example.demo.modelos.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 *
 * @author juan2
 */
@Service
public class NotificationConsumer {
    @Autowired
    private NotificationService notificationService;

    @KafkaListener(topics = "individual")
    public void receiveNotification(Notification notification) {
        switch(notification.getTipo()) {
            case "SMS":
                notificationService.sendSms(notification);
                break;
            case "EMAIL":
                notificationService.sendEmail(notification);
                break;
            case "WHATSAPP":
                notificationService.sendWhatsapp(notification);
                break;
            default:
                // Enviar a un canal por defecto o manejar el error
                break;
        }
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.example.demo.services;

import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import com.example.demo.configuration.TwilioConfig;
import com.example.demo.modelos.Notification;
import com.twilio.Twilio;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

/**
 *
 * @author juan2
 */
@Service
public class NotificationService {
    @Autowired
    private TwilioConfig twilioConfig;

    @Autowired
    private JavaMailSender emailSender;

    public void sendSms(Notification notification) {
        Twilio.init(twilioConfig.getAccountSid(), twilioConfig.getAuthToken());
        Message message = Message.creator(
            new PhoneNumber(notification.getDestinatario()),
            new PhoneNumber(twilioConfig.getPhoneNumber()),
            notification.getMensaje())
        .create();
    }

    public void sendEmail(Notification notification) {
        SimpleMailMessage message = new SimpleMailMessage(); 
        message.setFrom("your-email@gmail.com");
        message.setTo(notification.getDestinatario()); 
        message.setSubject("Notification");
        message.setText(notification.getMensaje());
        emailSender.send(message);
    }

    public void sendWhatsapp(Notification notification) {
        Twilio.init(twilioConfig.getAccountSid(), twilioConfig.getAuthToken());
        Message message = Message.creator(
            new com.twilio.type.PhoneNumber("whatsapp:" + notification.getDestinatario()),
            new com.twilio.type.PhoneNumber("whatsapp:" + twilioConfig.getPhoneNumber()),
            notification.getMensaje())
        .create();
    }
}
